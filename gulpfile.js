var gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	copy = require('gulp-copy');

var config = {
	bootstrapDir: './src/components/bootstrap-sass',
	publicDir: './dist',
};

gulp.task('css', function() {
	return gulp.src('./src/scss/styles.scss')
	.pipe(sass({
		includePaths: [config.bootstrapDir + '/assets/stylesheets'],
	}))
	.pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('fonts', function() {
	return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
	.pipe(gulp.dest(config.publicDir + '/fonts'));
});

gulp.task('scripts', function() {
	return gulp.src(['./src/components/jquery/dist/jquery.min.js', './src/js/**/*.js'])
	.pipe(concat('scripts.js'))
	.pipe(gulp.dest('./dist/js'));
});

gulp.task('html', function() {
	return gulp.src('./src/**/*.html')
	.pipe(copy(config.publicDir, {prefix: 1}));
});

gulp.task('default', ['css', 'fonts', 'scripts', 'html']);
